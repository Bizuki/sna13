from threading import Lock

from fastapi import FastAPI, HTTPException

stack_lock = Lock()
stack = []

app = FastAPI()

def locked(f):
    def wrap(*args, **kwargs):
        with stack_lock:
            return f(*args, **kwargs)
   
    return wrap

@locked
@app.get('/pop')
def pop():
    if not stack:
        raise HTTPException(status_code=404, detail='stack is empty')
    return stack.pop()


@locked
@app.get('/push/{item}')
def push(item: str):
    stack.append(item)
    
    return item


@locked
@app.get('/len')
def length():
    return len(stack)