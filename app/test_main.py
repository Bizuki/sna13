from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)

def check_len(stack):
    response = client.get('/len')
    assert response.status_code == 200
    assert response.content == str(len(stack)).encode('utf-8')


def pop(stack):
    check_len(stack)
    if len(stack) == 0:
        response = client.get('/pop')
        assert response.status_code == 404
    else:
        response = client.get('/pop')
        assert response.status_code == 200
        assert response.json() == str(stack.pop())
    check_len(stack)

       
def push(stack, item):
    item = str(item)
    check_len(stack)
    response = client.get(f'/push/{item}')
    assert response.status_code == 200
    assert response.json() == item
    stack.append(item)
    check_len(stack)


def test_intial_stack():
    stack = []
    pop(stack)
    push(stack, 2)
    push(stack, 3)
    push(stack, 'a')
    pop(stack)
    push(stack, 4)
    pop(stack)
    pop(stack)
    pop(stack)
    pop(stack)
